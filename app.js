const express = require("express");
const app = express();
const path = require("node:path");
const os = require("os");
const PORT = process.env.PORT || 8000;

app.set("view engine", "ejs");
require("dotenv").config();

app.get("/", (req, res) => {
	res.render("index", {
		containerID: os.hostname(),
		environment: process.env.ENVIRONMENT,
		stripeKey: process.env.STRIPE_KEY,
		appUrl: process.env.APP_URL,
	});
});

app.listen(PORT, () => {
	console.log(`Server Running on Port ${PORT}`);
});
